import sys
from os import scandir


def ls2(path):
    return [obj.name for obj in scandir(path) if obj.is_file()]


def main():
    if(len(sys.argv) > 4):
        files = ls2("src/routes/")
        # 1 nombre/archivo 2 metodo 3 variable DbFrom 4 dbWhere
        if((sys.argv[1])and(sys.argv[2])and(sys.argv[3])and(sys.argv[4])):
            # print(sys.argv)
            if((sys.argv[1]+"Routes.ts") in files):
                if((sys.argv[2] == "post") or (sys.argv[2] == "Post") or (sys.argv[2] == "delete") or (sys.argv[2] == "Delete") or (sys.argv[2] == "put") or (sys.argv[2] == "Put") or (sys.argv[2] == "get") or (sys.argv[2] == "Get")):
                    print(sys.argv)
                    if((sys.argv[2] == "post") or (sys.argv[2] == "Post")):
                        insert(sys.argv[1], sys.argv[3])
                    elif((sys.argv[2] == "delete") or (sys.argv[2] == "Delete")):
                        delete(sys.argv[1], sys.argv[3], sys.argv[4])
                    elif((sys.argv[2] == "put") or (sys.argv[2] == "Put")):
                        put(sys.argv[1], sys.argv[3], sys.argv[4])
                    elif((sys.argv[2] == "get") or (sys.argv[2] == "Get")):
                        get(sys.argv[1], sys.argv[3], sys.argv[4])
                else:
                    print(
                        "metodo no reconocido\n los reconocidos son: \n push\ndelete\nput \nget")
            else:
                print(
                    "El archivo al que se le deberia agregar el metodo no existe pendejo, escribe bien :v ")
    else:
        print("1 nombre/archivo\n2 metodo a agregar\n3 DbFrom \n4 DbWhere \n Ej: python dbActions.py prueba post docentes id_docente")


def insert(nArchivo, dbfrom):
    contenido = list()
    ins = True
    with open('src/controllers/%sController.ts' % nArchivo, 'r') as archivo:
        for linea in archivo:
            if('public async post(' in linea):
                linea += """\n\t\t\t //texto insertado por dbActions
            console.log(req.body);
            await pool.query('INSERT INTO %s set ?', [req.body]);
            res.json({ "texto": `insertado nuevo dato en %s.` });;\n\t\t\t//fin :v\n""" % (dbfrom, dbfrom)
            if('import pool from \'../database/database\'' in linea):
                ins = False
            contenido.append(linea)
        if(ins):
            contenido.insert(1, "import pool from '../database/database';\n")

    with open('src/controllers/%sController.ts' % nArchivo, 'w') as archivo:
        archivo.writelines(contenido)


def delete(nArchivo, dbFrom, dbWhere):
    ins = True
    contenido = list()
    with open('src/controllers/%sController.ts' % nArchivo, 'r') as archivo:
        for linea in archivo:
            if('public async delete(' in linea):
                linea += """\n\t\t\t //texto insertado por dbActions
        const { id } = req.params;
        const result = await pool.query('delete from %s where %s=? ', [id]);
        res.json({ "texto": `Se a eliminado el dato con el id:${id} de %s ` });\n\t\t\t//fin :v\n""" % (dbFrom, dbWhere, dbFrom)
            if('import pool from \'../database/database\'' in linea):
                ins = False
            contenido.append(linea)
        if(ins):
            contenido.insert(1, "import pool from '../database/database';\n")
    with open('src/controllers/%sController.ts' % nArchivo, 'w') as archivo:
        archivo.writelines(contenido)


def put(nArchivo, dbFrom, dbWhere):
    ins = True
    contenido = list()
    with open('src/controllers/%sController.ts' % nArchivo, 'r') as archivo:
        for linea in archivo:
            if('public async put(' in linea):
                linea += """\n\t\t\t //texto insertado por dbActions
        const { id } = req.params;
        await pool.query('update %s set ? where %s=? ', [req.body, id]);
        res.json({ texto: "%s actualizado: " + req.params.id });\t\t\t//fin :v\n""" % (dbFrom, dbWhere, dbFrom)
            if('import pool from \'../database/database\'' in linea):
                ins = False
            contenido.append(linea)
        if(ins):
            contenido.insert(1, "import pool from '../database/database';\n")
    with open('src/controllers/%sController.ts' % nArchivo, 'w') as archivo:
        archivo.writelines(contenido)


def get(nArchivo, dbFrom, dbWhere):
    ins = True
    contenido = list()
    with open('src/controllers/%sController.ts' % nArchivo, 'r') as archivo:
        for linea in archivo:
            if('public async getOne(' in linea):
                print(linea)
                linea += """\n\t\t\t //texto insertado por dbActions
        const { id } = req.params;
        const result = await pool.query('Select * from %s where %s=?', [id])
        if (result.length > 0) {
            return res.json(result);
        }
        res.status(404).json({ text: `Id:${id} no encontrado en %s` })\t\t\t//fin :v\n""" % (dbFrom, dbWhere, dbFrom)
            if('import pool from \'../database/database\'' in linea):
                ins = False
            contenido.append(linea)
        if(ins):
            contenido.insert(1, "import pool from '../database/database';\n")
    with open('src/controllers/%sController.ts' % nArchivo, 'w') as archivo:
        archivo.writelines(contenido)


main()
