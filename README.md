# Api

## Comenzando 🚀
_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._
* [ 1 ]  `git clone` 

* [ 2 ]  `cd miApi/`

* [ 3 ]  `npm install `
---
## **PUESTA EN MARCHA**
<br />
*dentro del proyecto*
<br />

* [ 4 ]  `npm run build`
-- o tambien 
`tsc`   esto genera el codigo javascript ejecutable ya que el proyecto está en typescript.

* [ 5 ]  `npm run dev`
-- o tambien 
`node build/index.js`  esta ya es la ejecucion del programa en si :v.

---
## **Rutas actualmente habilitadas de la api**

http://localhost:3000/ 

http://localhost:3000/api/teachers

---
## Generar nuevos accesos a la Api
### Generador.py
### Este programa crea el esqueleto de un nuevo acceso a la api en dependecia de ciertos parametos que le pide al usuario los cuales son:

*  `Nombre el cual se utilizara para la clase del acceso.`

*  `Ruta/url desde la cual se llamara al metodo get que tiene la clase.`


* *Por omision solo tiene metodo Get al ser solo un esqueleto. Pero con eso basta paara ser visible desde un navegador.*

---

## Crear metodos en los accesos ya creados

### MethodGen.py
### Este programa añade varios metdos como ´´put, get, delete, post´´ a los esqueletos creados con el programa anterior pero requiere 4 parametros: 
* *NombreDeLaClase* 
* *NombreDelArchivo*
* *Metodo* 
* *Parametro*

`Ej: python methodGen.py prueba prueba post /:id`


* *Por omision solo contiene el suficiente codigo para responder sus respectivas llamadas sin realizar nada mas que enviar un json que comprueba su funcionamiento.*
---
## Agregar sentencias Sql a los metodos
### dbActions.py
### Este programa añade la sentencia Sql previamente asociada a cada metodo  para asi poder generar un Crud de manera rapid.
###metodo previamente configurados
* *Metodo Get*: *Mostrar todos los datos de los campos en sus parametros*
* *Metodo Put*: *Editar el dato que fue pasado mediante un identificador*
* *Metodo Delete*: *Como su nombre lo dice este metodo lo que hace es borrar el dtao que se le pasa identificado por uno de sus parametros*
* *Metodo Post*: *Este metodo lo que hace es Ingresar o crear un dato en la la base de datos*

`Ej: python dbActions.py prueba post docentes id_docente`

* *En el ejemplo anterior lo que creamos es un metodo `Post` el cual esta en la clase `prueba` que a su vez tiene su respectiva `url de acceso` mediante la cual se le podra enviar los datos necesraios para `insertarlos` en la base de datos*