import sys
from os import scandir
def ls2(path): 
    return [obj.name for obj in scandir(path) if obj.is_file()]
def main():
    if(len(sys.argv) > 4):
        files=ls2("src/routes/")
        # 1 nombre de la clase a modificar 2 nombre/archivo 3 metodo 4 argumento
        if((sys.argv[1])and(sys.argv[2])and(sys.argv[3])and(sys.argv[3])):
            #print(sys.argv)
            if((sys.argv[2]+"Routes.ts") in files):
                if((sys.argv[3] == "post") or (sys.argv[3] == "Post") or (sys.argv[3] == "delete") or (sys.argv[3] == "Delete") or (sys.argv[3] == "put") or (sys.argv[3] == "Put") or (sys.argv[3] == "get") or (sys.argv[3] == "Get")):
                    inMetC(sys.argv[1], sys.argv[2], sys.argv[3])
                    inMetR(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
                else:
                    print("metodo no reconocido\n los reconocidos son: \n push\ndelete\nput \nget")
            else:
                print("El archivo al que se le deberia agregar el metodo no existe pendejo, escribe bien :v ")
    else:
        print("1 nombre de la clase a modificar\n2 nombre/archivo\n3 metodo a agregar\n4 argumento\n Ej: python methodGen.py prueba prueba post /:id")


def inMetC(nombre, temarchivo, metodo):
    if (metodo=='get'):
        metodo+='One' 
    print("se recibio ", nombre, temarchivo, metodo)
    contenido = list()
    with open('src/controllers/%sController.ts' % temarchivo, 'r') as archivo:
        for linea in archivo:
            if(("class %sController {\n"%nombre.capitalize())in linea):
                linea += """\t\tpublic async %s(req: Request, res: Response) {

                res.json({"texto":"el metodo %s en la funcion %s funciona"});

            }\n""" % (metodo, metodo, nombre)
            contenido.append(linea)
    with open('src/controllers/%sController.ts' % temarchivo, 'w') as archivo:
        archivo.writelines(contenido)


def inMetR(nombre, temarchivo, metodo, argumento):
    print("se recibio ", nombre, temarchivo, metodo, argumento)
    contenido = list()
    with open('src/routes/%sRoutes.ts' % temarchivo, 'r') as archivo:
        for linea in archivo:
            if((linea.replace(' ','') == "config():void{\n")):
                if (metodo=="get"):
                    linea += """\t\t\tthis.router.get('%s',%sController.getOne);\n""" % (
                    argumento, nombre)
                else:
                    linea += """\t\t\tthis.router.%s('%s',%sController.%s);\n""" % (
                    metodo, argumento, nombre, metodo)
            contenido.append(linea)
    with open('src/routes/%sRoutes.ts' % temarchivo, 'w') as archivo:
        archivo.writelines(contenido)


main()
