def main():
    print("Ingrese en nombre del acceso")
    nombre = input()
    print("Ingrese la ruta de del cual sera accedida la api: ")
    ruta = input()
    generarRuta(nombre)
    generarController(nombre,ruta)
    modifyIndex(nombre,ruta)
    # print("El acceso se llama:",nombre,"y se accedera a desde:",ruta)


def generarRuta(nombre):
    print("Los datos recividos son:", nombre)
    routes = '''import { Router } from 'express'
        import {%sController}   from '../controllers/%sController'

        class %sRoutes {
            public router: Router = Router();

            constructor() {
                this.config();
            }
            config():void{
                this.router.get('/',%sController.get);
            }

        }

        const %sRoutes =  new %sRoutes();
        export default %sRoutes.router;''' % (nombre, nombre, nombre.capitalize(), nombre, nombre, nombre.capitalize(), nombre)
    #print(routes)
    archivo = open("src/routes/%sRoutes.ts" % nombre, "w")
    archivo.write(routes)
    archivo.close()


def generarController(nombre,ruta):
    print("Los datos reciidos son:", nombre,ruta)
    controller = '''import { Request, Response} from 'express';
        class %sController {

        public async get(req: Request, res: Response) {

                res.json({"text":"the api \'%s\' in te route \'%s\' works"})
            }
        }

        export const %sController = new %sController();''' % (nombre.capitalize(), nombre, ruta, nombre, nombre.capitalize())
    #print(controller)
    archivo = open("src/controllers/%sController.ts" % nombre, "w")
    archivo.write(controller)
    archivo.close()


def modifyIndex(nombre, ruta):
    contenido = list()
    with open('src/index.ts', 'r') as archivo:
        count = 0
        for linea in archivo:
            if(count == 0):
                linea += "\nimport %sRoutes from './routes/%sRoutes'; \n" % (nombre,nombre)
            if(linea.replace(' ','')== "routes():void{\n"):
               linea += "\t\tthis.app.use('%s',%sRoutes); \n" % (ruta, nombre)
            count += 1
            contenido.append(linea)
    with open('src/index.ts', 'w') as archivo:
        archivo.writelines(contenido)


main()
