import { Request, Response } from 'express';
import pool from '../database/database'
class TeachersController {
    public async post(req: Request, res: Response) {
        console.log(req.body);
        await pool.query('INSERT INTO docentes set ?', [req.body]);
        res.json({ "texto": `insertado nuevo profesor.` });;

    }
    public async put(req: Request, res: Response): Promise<any> {
        const { id } = req.params;
        await pool.query('update docentes set ? where id_docente=? ', [req.body, id]);
        res.json({ texto: "Docente actualizado: " + req.params.id });
    }
    public async delete(req: Request, res: Response) {
        const { id } = req.params;
        const result = await pool.query('delete from docentes where id_docente=? ', [id]);
        res.json({ "texto": `Se a eliminado el profesor con el id:${id} ${result}` });

    }
    public async get(req: Request, res: Response) {
        const { id } = req.params;
        const result = await pool.query('Select * from docentes where id_docente=?', [id])
        if (result.length > 0) {
            return res.json(result);
        }
        res.status(404).json({ text: `Id:${id} no encontrado` })
    }

    public async teachers(req: Request, res: Response) {
        const result = await pool.query('Select * from docentes')
        res.json(result);
    }
}

export const teachersController = new TeachersController();