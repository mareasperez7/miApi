import express, { Application } from 'express';
import  morgan  from 'morgan';
import  cors  from 'cors';
import id_aulaRoutes from './routes/id_aulaRoutes'; 
import facultadesRoutes from './routes/facultadesRoutes'; 
import clasesRoutes from './routes/clasesRoutes'; 
import carrerasRoutes from './routes/carrerasRoutes'; 
import aulasRoutes from './routes/aulasRoutes'; 
import indexRoutes from './routes/indexRoutes';
import teachersRoutes from './routes/teachersRoutes';
class Server {
    public app: Application;
    constructor() {
        
        this.app = express();
        this.config();
        this.routes();
    }
    config(): void {
        this.app.set('port',process.env.PORT||3000);
        this.app.use(morgan('dev'));
        this.app.use(cors());
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended: false}));
        
    }
    routes(): void {
		this.app.use('/api/aula',id_aulaRoutes); 
		this.app.use('/api/facultades',facultadesRoutes); 
		this.app.use('/api/clases',clasesRoutes); 
		this.app.use('/api/carreras',carrerasRoutes); 
		this.app.use('/api/aulas',aulasRoutes); 
        this.app.use('/',indexRoutes);
        this.app.use('/api/teachers',teachersRoutes);
    }
    start(): void{
        this.app.listen(this.app.get('port'));
        console.log("server on port ",this.app.get('port'));
        
    };
};
const server =new Server();
server.start();