import { Router } from 'express'
        import {clasesController}   from '../controllers/clasesController'

        class ClasesRoutes {
            public router: Router = Router();

            constructor() {
                this.config();
            }
            config():void{
                this.router.get('/',clasesController.index);
            }

        }

        const clasesRoutes =  new ClasesRoutes();
        export default clasesRoutes.router;