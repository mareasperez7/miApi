import { Router } from 'express'
        import {id_aulaController}   from '../controllers/id_aulaController'

        class Id_aulaRoutes {
            public router: Router = Router();

            constructor() {
                this.config();
            }
            config():void{
                this.router.get('/',id_aulaController.index);
            }

        }

        const id_aulaRoutes =  new Id_aulaRoutes();
        export default id_aulaRoutes.router;