import { Router } from 'express'
import {teachersController}   from '../controllers/teachersController'

class TeachersRoutes {
    public router: Router = Router();

    constructor() {
        this.config();
    }
    config():void{
			this.router.post('/',teachersController.post);
			this.router.put('/:id',teachersController.put);
			this.router.delete('/:id',teachersController.delete);
			this.router.get('/:id',teachersController.get);
        this.router.get('/',teachersController.teachers);
    }

}

const teachersRoutes =  new TeachersRoutes();
export default teachersRoutes.router;