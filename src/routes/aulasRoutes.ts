import { Router } from 'express'
        import {aulasController}   from '../controllers/aulasController'

        class AulasRoutes {
            public router: Router = Router();

            constructor() {
                this.config();
            }
            config():void{
                this.router.get('/',aulasController.index);
            }

        }

        const aulasRoutes =  new AulasRoutes();
        export default aulasRoutes.router;