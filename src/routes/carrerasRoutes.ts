import { Router } from 'express'
        import {carrerasController}   from '../controllers/carrerasController'

        class CarrerasRoutes {
            public router: Router = Router();

            constructor() {
                this.config();
            }
            config():void{
                this.router.get('/',carrerasController.index);
            }

        }

        const carrerasRoutes =  new CarrerasRoutes();
        export default carrerasRoutes.router;