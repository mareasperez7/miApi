import { Router } from 'express'
        import {facultadesController}   from '../controllers/facultadesController'

        class FacultadesRoutes {
            public router: Router = Router();

            constructor() {
                this.config();
            }
            config():void{
                this.router.get('/',facultadesController.index);
            }

        }

        const facultadesRoutes =  new FacultadesRoutes();
        export default facultadesRoutes.router;